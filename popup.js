const LINK_FOLDER_NAME = 'biblio-link';
const IMPORT_FILE_LOCATION = 'https://bitbucket.org/renton_l/biblio-link/raw/master/bookmarks.json';

function findBookmarksFolder(rootNode, searchString)
{
    if(rootNode.url)
    {
        return null;
    }
    else if(rootNode.title.indexOf(searchString)>=0)
    {
        return rootNode;
    }
    for(var i=0; i<rootNode.children.length; i++)
    {
        var dest = findBookmarksFolder(rootNode.children[i], searchString);
        if(dest)
        {
            return dest;
        }
    }
    return null;
}

function addBookmarkFolder(parentNode, folderName, cb){
  return chrome.bookmarks.create(
    {
      'parentId': parentNode.id,
      'title': folderName
    },
    function(newFolder) {
      cb(newFolder);
    }
  );
}

function addBookmark(destFolder, bookmarkURL, bookmarktitle)
{
  chrome.bookmarks.create(
    {
      title:bookmarktitle,
      parentId:destFolder.id,
      url:bookmarkURL
    }
  );
}

function importBookmarks(rootnode, data, cb) {
  for (var i = 0; i < data.bookmarks.length; i++) {
    importNodeTree(rootnode, data.bookmarks[i]);
  }
  cb();
}

function importNodeTree(parentnode, item) {
  if (item.url) {
    addBookmark(parentnode, item.url, item.title)
  }

  if (item.items) {
    addBookmarkFolder(parentnode, item.title, function(newfolder) {
      for (var i=0; i < item.items.length; i++) {
        importNodeTree(newfolder, item.items[i]);
      }
    });
  }
}

function exportBookmarks(rootnode) {
  console.log('iter ', rootnode);
  var bookmarks = [];
  for(var i=0; i < rootnode.children.length; i++) {
    if (rootnode.children[i].children) {
      console.log(rootnode.children[i].children);
      bookmarks.push(
        {
          "title":rootnode.children[i].title,
          "items":exportBookmarks(rootnode.children[i])
        }
      );
    } else {
      bookmarks.push(
        {
          "title":rootnode.children[i].title,
          "url":rootnode.children[i].url
        }
      )
    }
  }
  return bookmarks;
}

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById("export").addEventListener("click", function(e) {
    document.getElementById("export").disabled = true;
    chrome.bookmarks.getTree(function(nodes) {
      bookmarkBarNode = nodes[0].children[0];
      rootnode = findBookmarksFolder(bookmarkBarNode, LINK_FOLDER_NAME);
      if (rootnode) {
        data = { "bookmarks": exportBookmarks(rootnode) };
        console.log(JSON.stringify(data));
        document.getElementById("export").disabled = false;
        jQuery("#json").html(JSON.stringify(data));
      }
    });
  });

  document.getElementById("sync").addEventListener("click", function(e) {
    document.getElementById("sync").disabled = true;
    chrome.bookmarks.getTree(
      function(nodes) {
        bookmarkBarNode = nodes[0].children[0];
        rootnode = findBookmarksFolder(bookmarkBarNode, LINK_FOLDER_NAME);
        if (rootnode !== null) {
          chrome.bookmarks.removeTree(rootnode.id);
        }

        rootnode = addBookmarkFolder(bookmarkBarNode, LINK_FOLDER_NAME, function(rootnode) {
          $.ajax({
              url: IMPORT_FILE_LOCATION,
              dataType: 'json',
              success: function(data){
                  importBookmarks(rootnode, data, function() {
                    console.log('All Done!');
                    document.getElementById("sync").disabled = false;
                  });
              }
          });
        });
      }
    );
  });
});
